var Subscribe = (function(){
    var global = this,
        Event,
        _default = 'default';
    Event = (function(){
        var _listen,
            _trigger,
            _remove,
            _slice = Array.prototype.slice,
            _shift = Array.prototype.shift,
            _unshift = Array.prototype.unshift,
            namespaceCache = {},
            _create,
            find,
            each = function(ary,fn){
                var ret;
                for(var i=0,l=ary.length;i<l;i++){
                    var n= ary[i];
                    ret = fn.call(n,i,n);
                };
            };

        _listen = function(key,fn,cache){
            if(!cache[key]){
                cache[key] = [];
            };
            cache[key].push(fn);
        };
        _remove = function(key,cache,fn){
            if(cache[key]){
                if(fn){
                    var index = cache[key].indexOf(fn);
                    if(index > -1){
                        cache[key].splice(index,1);
                    };
                    // for(var i = cache[key].length;i>=0;i--){
                    //     if(cache[key][i] === fn){
                    //         cache[key].splice(i,1);  
                    //     };
                    // };
                }else{
                    cache[key] = [];
                };
            };
        };
        _trigger = function(){
            var cache = _shift.call(arguments),
                key = _shift.call(arguments),
                args = arguments,
                _self = this,
                ret,
                stack = cache[key];
            var fn = args.length ? args[args.length - 1] : null;
            
            if(!stack || !stack.length){
                return;
            };
            if(fn && stack.indexOf(fn) > -1){ //如果最后个值是函数 且 stack里有该函数，则只执行该函数
                var _fn = Array.prototype.pop.call(args);
                return _fn.apply(_self,args);
            };

            return each(stack,function(){
                return this.apply(_self,args);
            });
        };
        _create = function(namespace){
            var namespace = namespace || _default;
            var isRun = true; //是否正在执行
            var cache = {},
                offlineStack = [],
                pauseStack = [],
                ret = {
                    listen: function(key,fn,last){
                        _listen(key,fn,cache);
                        if(offlineStack === null){
                            return;
                        };
                        if(last === 'last'){
                            offlineStack.length && offlineStack.pop()();
                        }else{
                            each(offlineStack,function(){
                                this();
                            });
                        };
                        offlineStack = null;
                    },
                    _one: function(key,fn,last){
                        _remove(key,cache);
                        this.listen(key,cache,fn);
                    },
                    remove: function(key,fn){
                        _remove(key,cache,fn);
                    },
                    trigger:function(){
                        var fn,
                            args,
                            _self = this;
                        if(!isRun){ //如果正在暂停
                            return pauseStack.push((function(args){
                                return function(){
                                    this.trigger.apply(_self,args);
                                }
                            })(arguments));
                        };
                        _unshift.call(arguments,cache);
                        args = arguments;
                        fn = function(){
                            return _trigger.apply(_self,args);
                        };
                        if(offlineStack){
                            return offlineStack.push(fn);
                        };
                        return fn();
                    },
                    pause: function(){
                        isRun = false;
                    },
                    run: function(){
                        var _self = this;
                        isRun = true;
                        each(pauseStack,function(){
                            return this.apply(_self);
                        });
                        pauseStack = [];
                    }
                };
                return  namespace ? 
                        (namespaceCache[namespace] ? namespaceCache[namespace] : namespaceCache[namespace] = ret)
                        : ret;
        };
            
        return {
            create: _create,
            one: function(key,fn,last){
                var event = this.create();
                event.one(key,fn,last);
            },
            remove: function(key,fn){
                var event = this.create();
                event.remove(key,fn);
            },
            listen: function(key,fn,last){
                var event = this.create();
                event.listen(key,fn,last);
            },
            trigger: function(){
                var event = this.create();
                event.trigger.apply(this,arguments);
            }
        };
    })();

    return Event;
})();



//明日方舟材料计算
function MaterialCalculator(Material,needNum){
    var getResult = function(){
        var cache = [];
        var _Material = Material;
        while(_Material){
            if(_Material.material){
                cache.push((function(ma){
                    return function(_mc){
                        return ma.count + (Math.floor(_mc / ma.needMaterialNum));
                    }
                })(_Material));
            }else{
                cache.push((function(ma){
                    return function(){
                        return Math.floor(ma.count)
                    };
                })(_Material));
            };
            
            _Material = _Material.material;
        };
        cache.reverse();
        var prevResult;
        cache.forEach(v=>{
            prevResult = v(prevResult);
        });
        return prevResult;
    };
    var result = getResult();
    console.log('result',result);
    for(var i =0;i<99;i++){
        if(result >= needNum){
            break;
        }
        var _Material = Material;
        while(_Material){
            _Material.count += _Material.increase;
            _Material = _Material.material;
        };
        result = getResult();
        console.log(`第${i+1}把,一共可以做${result}个。`);
    }
}

function Material(increase,current,source,num){
    this.count = current || 0;
    this.material = source;
    this.needMaterialNum = source && num ? num : 0;
    this.increase = increase || 0;
};

var a1 = new Material(0.3490,0);

// MaterialCalculator(a1,10);
// var a1=0, //提纯源石
//     a2=1, //固源岩组
//     a3=8, //固原岩
//     a4=1, //源岩
//     count = 0; //场数
// var ga1 = function(_g2){
//     // return new Promise(res=>{
//     //     res(Math.floor(_g2/3));
//     // });
//     return Math.floor(a1 + (_g2/3));
// };
// var ga2 = function(_g3){
//     // return new Promise(res=>{
//     //     res(Math.floor(_g3/3));
//     // });
//     return Math.floor(a2 + (_g3/3));
// };
// var ga3 = function(_g4){
//     // return new Promise(res=>{
//     //     res(Math.floor(_g4/3));
//     // });
//     return Math.floor(a3 + (_g4/3));
// };
// var result = function async(){
//     // ga3(a4).then(data=>ga2(data)).then(data=>ga1(data)).then(data=>{
//     //     console.log(data+a1);
//     // });
//     var _a3 = ga3(a4);
//     var _a2 = ga2(_a3);
//     var _a1 = ga1(_a2);
//     console.log(count,_a3,_a2,_a1);
// };

// for(var i=0;i<50;i++){
//     a1+= 0.0507;
//     a2+= 0.4524;
//     a3+= 0.3082;
//     a4+= 0.2869;
//     count +=1;
//     // console.log(a1,a2,a3,a4)
//     result();
// }
// while()


// Node.js
if (typeof module === 'object' && module.exports) {
    module.exports = Subscribe;
}
// AMD / RequireJS
else if (typeof define === 'function' && define.amd) {
    define([], function () {
        return Subscribe;
    });
};