
// var step1 = ['电棍','剧毒','小黑','小小','巨牙海民','敌法师',11111111111111111111111111
// '斧王','暗影萨满','术士','小鹿','蝙蝠骑士','血魔','赏金','蓝胖111']; 

// var step2 = ['主宰','伐木机','先知','兽王','水人','巫医','帕克','帕吉',
//   '大鱼','混沌','女王','莉娜','露娜','风行者'];

// var step3 = ['小鱼'];

// var arr1 = new RandomItem(step1);
// var arr2 = new RandomItem(step2);
// var arr3 = new RandomItem(step3);

// var attr = new RandomItem(['攻击成功']);
// var miss = new RandomItem(['闪避成功'])

// var arr = new RandomItem({
//   0.3: attr,
//   0.7: miss,
// });
// for(var i = 0 ;i<50;i++){
//   console.log(randomizer(1,arr) + Math.random()); 
// }

function randomizer(resultNum,resultArr){
    resultNum = typeof resultNum === 'number' ? Math.abs(resultNum) : 1;
    resultArr = resultArr && resultArr.constructor === RandomItem ? resultArr : new RandomItem();

    var result = [];
    var randomFn = getRandomFn(resultArr);

    for(var i =0;i<resultNum;i++){
        /*
            分为数组可重复，数组不可重复，对象可重复，对象不可重复
            数组不可重复，每次随机后清除随机项。
            对象可重复，随机前先分配好每个随机项取值范围，是个函数，类似(r)=>r>0 && r<10（10%），
            对象不可重复，除了上述操作外还要清楚随即项  
        */
       var _result = randomFn(Math.random());
       result.push(_result);
       randomFn = getRandomFn(resultArr,_result);
    };
    result = result.map(v=>{
        if(v && v.constructor === RandomItem){
            return randomizer(1,v);
        };
        return v;
    });
    return resultNum > 1 ? result : result.length ? result[0] : undefined;
};
function Copy(data){
    return JSON.parse(JSON.stringify(data));
};
function RandomItem(list,repeatable){
    this.list = list || [];
    this.repeatable = typeof repeatable === 'boolean' ? repeatable : true;
};
function getRandomResult(){
    
}
function getRandomFn(arrItem,selected){
    arrItem = arrItem && arrItem.constructor === RandomItem ? arrItem : new RandomItem();
    // arrItem = new RandomItem(Copy(arrItem.list),arrItem.repeatable);
    if(Array.isArray(arrItem.list)){ //如果是数组
        if(selected && !arrItem.repeatable){
            var index = arrItem.list.indexOf(selected);
            if(index > -1){
                arrItem.list.splice(index,1);
            };
        };
        return (function(arr){
            return function(random){
                random = typeof random === 'number' ? random : Math.random();
                var num =Math.round(random*(arr.length-1));
                return arr[num];
            };
        })(arrItem.list);
    }else if(!Array.isArray(arrItem.list) && typeof arrItem.list === 'object'){ //如果是对象
        if(selected && !arrItem.repeatable){
            for(var i in arrItem.list){
                if(arrItem.list[i] === selected){
                    Reflect.deleteProperty(arrItem.list,i);
                };
            };
        };
        return (function(arr){
            var count = 0;
            var cache = [];
            for(var i in arr){
                var _i = i.split(',').shift()*100;
                if(!isNaN(_i)){
                    cache.push((function(num,arr){
                        var countMin = count,countMax = count + num;
                        count += num;
                        return function(_num){
                            if(_num >= countMin && _num < countMax){
                                return arr;
                            };
                        };
                    })(_i,arr[i]));
                };
            };
            return function(random){
                random = typeof random === 'number' ? random : Math.random();
                var result;
                if(!cache.length){
                    return;
                };
                while(!result){
                    var num =Math.round(random*99);
                    for(let i = 0,l=cache.length;i<l;i++){
                        result = cache[i](num);
                        if(result){
                            break;
                        };
                    };
                    random = Math.random();
                };
                return result;
            };
        })(arrItem.list);
    };
};
// var resultNum

// Node.js
if (typeof module === 'object' && module.exports) {
    module.exports = {randomizer,RandomItem};
}
// AMD / RequireJS
else if (typeof define === 'function' && define.amd) {
    define([], function () {
        return {randomizer,RandomItem};
    });
};