import React from 'react';
import './main.css';
import {player} from '../../public/initialization.js';
import * as PIXI from 'pixi.js';

function getStar(length){
  var cache = [];
  var result = "";
  for(var i=0;i<length;i++){ 
    cache.push(i);
  };
  cache.map(v=>{
    result += "★"
  });
  return result;
}
function RecruitUnit(props){
  if(props.show){
    var list = props.recruitUnit.map((v,index)=>{
      if(v){
        return (
          <div className="aUnit" key={index} onClick={props.purchaseUnit.bind(this,v)}>
              {v.name} <br/> ${v.cost} <br /> {getStar(v.level)}
          </div>
          ); 
      }else{
        return (
          <div className="aUnit" key={index}></div>
          );
      }
    })
    return (
      <div className="recruitUnit">
        <div className="close" onClick={props.recruitClose}>×</div>
          {list}
          <div className="tools">
              <button onClick={props.refreshRecruitUnit}>重新招募1</button>
          </div>
      </div>
    )
  }else{
    return (<div></div>);
  }
}

function BenchUnit(props){
  var list = props.benchUnit.map((v,index)=>{
    return (
      <div className="aUnit" key={index} onClick={props.detailOpen.bind(this,v)}>
          {v.name} <br/> {getStar(v.level)}
      </div>
    ); 
  })
  return (
    <div className="benchField">
        {list}
    </div>
  )
}

// this.name = "未知";
// this.level = 1;
// this.nextLevel = null; //下一个升级对象
// this.hp = 650; //血量上限
// this.mp = 100; //魔法上限
// this.attack = 60; //攻击力
// this.attackSpeed = 1; //攻击频率
// this.attackType = 0; //攻击类型 0近战 1远程
// this.attackDistance = 1; //攻击距离
// this.defense = 5; //防御力
// this.move = 300; //移动速度
// this.recover = 5; //回血速度 /s
// this.magicResistance = 0; //魔法抗性
// this.equip = []; //装备
// this.yoke = []; //羁绊
// this.skill = []; //技能
// this.maxEquipNum = 1; //最大携带装备数量
// if(unit){
//     this.cost = unit.cost;
// };
function UnitDetail(props){
  if(props.unit && props.showDetail){
    var yoke = props.unit.yoke.map((v,i)=>{
      return <span key={i}>{v.name}</span>
    })
    var skill = props.unit.skill.map((v,i)=>{
      return <span key={i}>{v.name}</span>
    })
    var equip = props.unit.equip.map((v,i)=>{
      return <span key={i}>{v.name}</span>
    })
    var star = getStar(props.unit.level);
    return (
      <div className="unitDetail">
          <div className="close" onClick={props.detailClose}>×</div>
          <div>{props.unit.name}</div>
          <div>{star}</div>
          <div>血量：{props.unit.hp}/{props.unit.hpMax}</div>
          <div>魔法：{props.unit.mp}/{props.unit.mpMax}</div>
          <div>攻击力：{props.unit.attack}</div>
          <div>防御力：{props.unit.defense}</div>
          <div>魔法抗性：{props.unit.magicResistance}</div>
          <div>攻击速度：{props.unit.attackSpeed}</div>
          <div>攻击距离：{props.unit.attackDistance}</div>
          <div>移动速度：{props.unit.move}</div>
          <div>回血速度：{props.unit.recover}</div>
          <div>羁绊：{yoke}</div>
          <div>技能：{skill}</div>
          <div>装备：{equip}</div>
          <div><button onClick={props.sellUnit.bind(this,props.unit)}>出售</button> </div>
      </div>)
  }else{
    return null;
  }
}
class Main extends React.Component {
  recruitOpen(){
    this.setState({showRecruitUnit:true,});
  };
  recruitClose(){
    this.setState({showRecruitUnit:false});
  };
  detailOpen(unit){
    this.setState({
      showDetail:true,
      unitDetail: unit
    });
  }
  detailClose(){
    this.setState({showDetail:false});
  }
  purchaseUnit(v){
    player.purchaseUnit.call(player,v);
    this.refreshState();
    // this.setState({
    //   recruitUnit: player.recruitUnit,
    //   benchUnit: player.benchUnit,
    //   battlefieldUnit: player.battlefieldUnit,
    //   gold: player.gold
    // }); 
  };
  refreshRecruitUnit(){
    player.refreshRecruitUnit.call(player);
    this.refreshState();
    // this.setState({
    //   recruitUnit: player.recruitUnit,
    // });
  }
  sellUnit(unit){
    player.sellUnit.call(player,unit);
    this.refreshState();
    this.setState({
      showDetail: false,
      unitDetail: null
    })
  };

  refreshState(){
    this.setState({
      recruitUnit: player.recruitUnit,
      benchUnit: player.benchUnit,
      battlefieldUnit: player.battlefieldUnit,
      gold: player.gold,
      level: player.level,
      exp: player.exp,
      max: player.max,
      gold: player.gold,
      unitChance: player.unitChance,
    }); 
  }
  getExp(){
    player.getExp(player.level,player.exp,4,5); 
    this.refreshState();
    // this.setState({
    //   gold:player.gold,
    //   level: player.level,
    //   exp: player.exp,
    //   max: player.max,
    //   gold: player.gold,
    //   unitChance: player.unitChance
    // });
  };
  constructor(props){
    super(props);
    this.recruitOpen = this.recruitOpen.bind(this);
    this.recruitClose = this.recruitClose.bind(this);
    this.detailOpen = this.detailOpen.bind(this);
    this.detailClose = this.detailClose.bind(this);
    this.refreshRecruitUnit = this.refreshRecruitUnit.bind(this);
    this.purchaseUnit = this.purchaseUnit.bind(this);
    this.getExp = this.getExp.bind(this);
    this.sellUnit = this.sellUnit.bind(this);
    this.state = {
        showRecruitUnit: false,
        showDetail: false,
        detailUnit: null,
        recruitUnit: player.recruitUnit,
        level: player.level,
        exp: player.exp,
        max: player.max,
        gold: player.gold,
        unitChance: player.unitChance,
        benchUnit: player.benchUnit,
        battlefieldUnit: player.battlefieldUnit,
    };
  };

  render() {
    var showRecruitUnit = this.state.showRecruitUnit;
    var showDetail = this.state.showDetail;
    var recruitUnit = this.state.recruitUnit;
    var state = this.state;
    var chance = state.unitChance.map((v,i)=>{
      return  (<span key={i}>第{i+1}阶梯：{v*100}%；</span>)
    });
    // 此语法确保 `handleClick` 内的 `this` 已被绑定。
    return (
      <div className="body">
          <div className="wrap">
              <div className="battleField">

              </div>
              <BenchUnit  benchUnit={state.benchUnit} detailOpen={this.detailOpen} />
          </div>
          <div className="playerInfo">
            玩家等级：{state.level} ({state.exp}/{state.max}) <br />
            单位出现概率：{chance} <br />
            拥有金钱：{state.gold} <br />
            <button onClick={this.recruitOpen}>打开招募面板</button>
            <button onClick={this.getExp}>升级</button>
          </div>
          <RecruitUnit show={showRecruitUnit} recruitUnit={recruitUnit} recruitClose={this.recruitClose} 
              refreshRecruitUnit={this.refreshRecruitUnit} purchaseUnit={this.purchaseUnit}  />
          <UnitDetail unit={state.unitDetail} showDetail={showDetail} sellUnit={this.sellUnit} detailClose={this.detailClose} />
      </div>
    );
  };
};


export default Main;
