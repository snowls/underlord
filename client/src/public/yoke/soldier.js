import {Yoke} from './common.js';
class Soldier extends Yoke{
    name = "战士";
    describe = "羁绊要求：3/6/9；效果：增加5/7/9点护甲";

    constructor(unit){
        super();
        
        this.unit = unit;
    };

    getYokeEffect(num){
      if(num >=9){
        return this.yoke3Effect;
      }else if(num >= 6){
        return this.yoke6Effect;
      }else if(num >= 3){
        return this.yoke3Effect;
      };
    }
    getYokeResumeEffect(num){
      if(num >=9){
        return this.yoke9Resume;
      }else if(num >= 6){
        return this.yoke6Resume;
      }else if(num >= 3){
        return this.yoke3Resume;
      };
    }

    yoke3Effect(){
      this.defense += 5;
    }
    yoke3Resume(){
      this.defense -=5;
    }
    yoke6Effect(){
      this.defense += 10;
    }
    yoke6Resume(){
      this.defense -=10;
    }
    yoke9Effect(){
      this.defense += 15;
    }
    yoke9Resume(){
      this.defense -=15;
    }
};

export {Soldier};