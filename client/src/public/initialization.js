import {Player} from './player/player.js';
import {Global} from './global/global.js';
import {Chessboard} from './chessboard/chessboard.js';

var player = new Player();
var global = new Global([player]);
var chessboard = new Chessboard();
export {global,player};