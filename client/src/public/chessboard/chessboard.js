import * as PIXI from 'pixi.js';
class Chessboard{
    width = 1200; 
    height = 700;
    mainWidth; //主场宽
    mainHeight; //主场高
    app; //总pixi对象
    mainField; //主场pixi对象
    benchField; //板凳pixi对象
    texture;
    constructor(){
        this.app = new PIXI.Application({
            width: this.width,
            height: this.height,
            // antialias: true, //抗锯齿 
            transparent: false,
            resolution: 1
        });
        document.body.appendChild(this.app.view);
        const loader = new PIXI.Loader();
        console.log('loader',loader,loader.add);
        loader
          .add('http://localhost:8080/assets/texture.json')
          .add('http://localhost:8080/assets/box.gif')
          .load(()=>{
            this.texture = PIXI.utils.TextureCache;
            this.allocation();
            // let grassland = new PIXI.Sprite(texture['grassland.gif']);
            // this.app.stage.addChild(grassland); 

            // let wall1 = new PIXI.Sprite(texture['wall1.gif']);
            // wall1.x = 32;
            // this.app.stage.addChild(wall1); 
            // let wall2 = new PIXI.Sprite(texture['wall2.gif']);
            // wall2.x = 64;
            // this.app.stage.addChild(wall2); 
          });
    };

    allocation(){ //分配空间
      let width = this.width;
      let height = this.height;

      let benchField = this.createMainField([0,0],[width,150]);
      benchField.container.position.set(0,height - 150);
      this.app.stage.addChild(benchField.container);
      this.benchField = benchField.container_i;

      let bian = Math.min(width,height-150);
      bian = bian - bian%32;
      let mainField = this.createMainField([0,0],[bian,bian]);
      this.app.stage.addChild(mainField.container);
      this.mainField = mainField.container_i;
      this.createChessTexture();
    };

    createChessTexture(){ //创建棋盘纹理
      let width = this.mainField.width;
      let height = this.mainField.height;
      let increase_x = width/8;
      let increase_y = height/8;
      let s_x=0,s_y=0;
      let bool = true;

      for(;s_y<height;s_y+=increase_y){
        s_x = 0;
        bool = !bool;
        for(;s_x<width;s_x+=increase_x){
          let rectangle = new PIXI.Graphics();
          rectangle.beginFill(0x32CD32,bool ? 0.5 : 0.1);
          // rectangle.drawRect(s_x,s_y,increase_x,increase_y);
          rectangle.drawRect(0,0,increase_x,increase_y);
          rectangle.endFill();
          rectangle.x = s_x;
          rectangle.y = s_y;
          rectangle.interactive = true;
          rectangle.on('mouseover', (event) => {
            console.log('mouseover',event);
          });
          
          rectangle.on('mouseout', (event) => {
            console.log('mouseout',event);
          });
          this.mainField.addChild(rectangle);
          bool = !bool;
        };
      };
      
      // let rectangle = new PIXI.Graphics();
      // rectangle.beginFill(0xFFFF00,0.3);
      // rectangle.drawRect(0,height/2,width,height/2);
      // rectangle.endFill();
      // this.mainField.addChild(rectangle);
    };

    createMainField(start,end){ //创建地形
      let s_x = start[0],s_y = start[1];
      let e_x = end[0],e_y = end[1];
      let start_x = s_x,start_y = s_y;
      let container = new PIXI.Container();
      for(;start_y+32 < e_y;start_y+=32){
        start_x = s_x;
        for(;start_x+32 < e_x;start_x+=32){
          var grass = new PIXI.Sprite(this.texture['grassland.gif']);
          grass.x = start_x;
          grass.y = start_y;
          container.addChild(grass);
          if(start_x === s_x || start_x+64 >= e_x){ //左右石头
            var wall;
            if(start_y + 64 >= e_y){
              wall = new PIXI.Sprite(this.texture['wall2.gif']);
            }else{
              wall = new PIXI.Sprite(this.texture['wall1.gif']);
            };
            wall.x = start_x;
            wall.y = start_y;
            container.addChild(wall);
          }else if(start_y === s_y && (start_x >= 32 || start_x + 96 >= e_x)){ //上边石头
            var wall = new PIXI.Sprite(this.texture['wall2.gif']);
            wall.x = start_x;
            wall.y = start_y;
            container.addChild(wall);
          }else if(start_y+64 >= e_y && (start_x >= 32 || start_x + 96 >= e_x)){
            var wall = new PIXI.Sprite(this.texture['wall2.gif']);
            wall.x = start_x;
            wall.y = start_y;
            container.addChild(wall);
          };
        };
      };
      let container_i = new PIXI.Container();
      let rectangle = new PIXI.Graphics();
      // rectangle.beginFill(0x66CCFF,0.5);
      rectangle.drawRect(0,0,container.width-64,container.height-64);
      // rectangle.endFill();
      container_i.addChild(rectangle);
      container_i.x = s_x+32;
      container_i.y = s_y+32;
      container.addChild(container_i);
      return {container,container_i};
    };
    
    
};

export {Chessboard};