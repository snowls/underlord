import {Item} from './common.js';

class Dajian extends Item{
    carrier;
    constructor(){
        this.name = "大剑";
        this.describe = "这只是一把普通的剑";
        this.level = 1;
        this.type = 0;
    };

    putOnCallback(unit){
        unit.attack += 21;
    };

    takeOffCallback(unit){
        unit.attack -= 21;
    }
}

export {Dajian};