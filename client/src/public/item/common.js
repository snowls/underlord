class Item {
    carrier;
    constructor(){
        this.name = "未知"; //道具名称
        this.describe = ""; //描述
        this.type = 0; //0是普通道具，1是通用道具
        this.level = 1; //阶级
        if(this.type === 1){
            this.commonItemCallback();
        };
    };
    
    putOn(unit){ //装备
        this.takeOff();
        unit.equip.push(this);
        this.putOnCallback(unit);
        this.carrier = unit;
        unit.updateItemBattleCallback();
        // this.putOn(unit);
    };

    takeOff(){ //脱下
        if(this.carrier){
            var index = this.carrier.equip.indexOf(this);
            if(index > -1){
                this.carrier.equip.splice(index,1);
            };
        };
        this.takeOffCallback(this.carrier);
        this.carrier.updateItemBattleCallback();
        this.carrier = null;
    };
    
    putOnCallback(unit){ //装备时回调

    };

    takeOffCallback(unit){ //脱下装备回调

    };

    battleCallback(){ //战斗开始时回调

    };
    
    commonItemCallback(){ //通用物品回调

    }
}

export {Item};