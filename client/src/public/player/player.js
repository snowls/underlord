import {unitStep1,unitStep2,unitStep3,unitStep4,unitStep5} from '../../public/unit/unit.js';
import {getExp,getUnitChance} from './level.js';

var subscribe = require('subscribe');
var {randomizer,RandomItem} = require('randomizer');

class Player{
    gold = 999; //金钱
    level = 1; //等级
    exp = 0; //经验
    max = 0; //最大经验值
    upgradeable = true; //是否可升级
    unitChance = []; //各阶级单位出现概率
    
    recruitUnit = []; //招募列表
    battlefieldUnit = []; //上场单位
    benchUnit = []; //板凳上的单位
    benchMax = 8; //板凳单位最大数量
    

    constructor(){
        this.unitChangeSubscribe = subscribe.create('unitChange');
        this.unitChangeSubscribe.listen('unitChange',this.unitChange);
        this.getExp(this.level,this.exp,0);
        this.refreshRecruitUnit();
    };

    unitChange(unitArr){ 
        /*先过滤掉同种单位，再统计每个羁绊的数量，生成一个对象，key是羁绊.name，vallue是数量。
        最后再循环所有羁绊，从对象中获取值。*/
      var unitCache = [],
            yokeCache = [],
            result = {};
        unitArr.forEach(v=>{ //过滤同种单位
            var isExist = false;
            for(var i=0,l=unitCache.length;i<l;i++){
                if(unitCache[i].constructor === v.constructor){
                    isExist = true;
                    break;
                };
            };
            if(!isExist){
                unitCache.push(v);
                yokeCache.push(...v.yoke);
            };
        });
        yokeCache.forEach(v=>{ //统计 
            if(!result[v.name]){
                result[v.name] = 1;
            }else{
                result[v.name]+=1;
            };
        });
        unitArr.forEach(v=>{
            var battleCallback= [];
            var battleResumeCallback = [];
            v.yoke.forEach(vv=>{
                battleCallback.push(vv.getYokeEffect(result[vv.name]).bind(v));
                battleResumeCallback.push(vv.getYokeResumeEffect(result[vv.name]).bind(v));
            });
            v.updateBattleCallback(battleCallback);
            v.updateResumeBattleCallback(battleResumeCallback);
        });
    };

    purchaseUnit(unit){ //购买单位
        if(!unit){
            return;
        }
        if(this.gold < unit.cost){
            alert('金钱不足！');  
            return;
        };
        var compose = this.sureCompose(unit),
            length = compose.length + 1;
        if(length < 3 && this.benchUnit.length >= this.benchMax){ 
            alert('板凳位置不够！');
            return;
        };
        this.recruitUnit.splice(this.recruitUnit.indexOf(unit),1,undefined);

        if(length < 3){
            this.benchUnit.push(unit);
        }else{
            this.composeUnit(compose,length);
        };
        this.gold -= unit.cost;
        this.unitChangeSubscribe.trigger('unitChange',this.battlefieldUnit); //下发通知，参数为上场单位
    };

    composeUnit(compose,length){//合成单位
        while(compose.length >=3 || length >= 3){
            console.log('composeUnit',compose,length);
            //获取位置和装备，因为没写棋盘系统，所以暂时不获取位置。 
            var newUnit = new compose[0][1].nextLevel(compose[0][1]);
            var itemCache = [];
            compose.reverse().forEach(v=>{
                if(v){
                    var _unit = v[1];
                    if(_unit.equip.length){
                        itemCache = [..._unit.equip];
                    }; 
                    _unit.equip.forEach(vv=>{
                        vv.takeOff();//脱下装备
                    });
                };
            });
            itemCache.forEach(v=>{ 
                v.putOn(newUnit);//新单位穿上装备
            });
            compose.forEach(v=>{ //删除旧单位
                if(v){
                    var index = v[0].indexOf(v[1]);
                    if(index > -1){
                        v[0].splice(index,1);
                    };
                }
            });
            compose[0][0].push(newUnit); //升级单位加入队伍
            compose = this.sureCompose(newUnit);
            length = compose.length;
        };
    };
    sureCompose(unit){ //返回相同单位列表
        var result = [];
        if(!unit.nextLevel){ //如果没有下一级
            return result;
        }
        this.battlefieldUnit.forEach((v,i)=>{
            if(v.constructor === unit.constructor){
                // result.push([this.battlefieldUnit,i]);
                result.push([this.battlefieldUnit,this.battlefieldUnit[i]]);
            };
        });
        this.benchUnit.forEach((v,i)=>{
            if(v.constructor === unit.constructor){
                // result.push([this.benchUnit,i]);
                result.push([this.benchUnit,this.benchUnit[i]]);
            };
        });
        return result.slice(0,3);
    };

    sellUnit(unit){ //出售单位
        var price = unit.getSellPrice();
        var index;
        if((index = this.battlefieldUnit.indexOf(unit)) > -1){
            this.battlefieldUnit.splice(index,1);
        }else if((index = this.benchUnit.indexOf(unit)) > -1){
            this.benchUnit.splice(index,1);
        };
        this.gold += price;
    };

    moveUnit(){ //移动单位，以后做

    };
    
    refreshRecruitUnit(level){ //刷新招募单位列表
        var step = {
            step1: new RandomItem(unitStep1),
            step2: new RandomItem(unitStep2),
            step3: new RandomItem(unitStep3),
            step4: new RandomItem(unitStep4),
            step5: new RandomItem(unitStep5)
        };
        var arr = {};
        this.unitChance.forEach((v,i)=>{
            arr[v + ',' + i] = step['step'+(i+1)];
        });
        console.log('refreshRecruitUnit',this.unitChance,arr);
        arr = new RandomItem(arr);
        var randomResult = [],
            recruitUnit = [];
        randomResult = randomizer(5,arr);
        randomResult.forEach(v=>{ 
            recruitUnit.push(v());
        });
    
        this.recruitUnit = recruitUnit;

    }

    getExp(level,exp,increase,gold){
        if(!this.upgradeable){
            alert('已达到最高级！');
            return;
        };
        if(this.gold - gold <= 0){
          alert('金钱不足！');
          return;
        }
        gold = gold || 0;
        var result = getExp(...arguments);
        this.level = result.level;
        this.exp = result.exp;
        this.max = result.max;
        this.upgradeable = result.upgradeable;
        this.unitChance = result.unitChance;
        this.gold -= gold;
    }

};

export {Player};