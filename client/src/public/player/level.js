var maxExp = {
    1:1,
    2:2,
    3:4,
    4:8,
    5:10,
    6:12,
    7:14,
    8:16,
    9:18,
};
function getUnitChance(level){
    switch(level){
        case 1:
            return [1,0,0,0,0];
        case 2:
            return [0.7,0.3,0,0,0];
        case 3:
            return [0.5,0.4,0.1,0,0];
        case 4:
            return [0.4,0.3,0.3,0,0];
        case 5:
            return [0.3,0.3,0.3,0.1,0];
        case 6:
            return [0.2,0.3,0.3,0.2,0];
        case 7:
            return [0.2,0.2,0.3,0.3,0];
        case 8:
            return [0.2,0.2,0.25,0.3,0.05];
        case 9:
            return [0.15,0.2,0.3,0.25,0.1];
        case 10:
            return [0.15,0.15,0.25,0.3,0.15];
        case 11:
            return [0.1,0.15,0.2,0.35,0.2];
    };
};

function getExp(level,exp,increase){
    var result = {
        level: level,
        exp: exp,
        max: maxExp[level] || 0,
        upgradeable: !!maxExp[level],
        unitChance: getUnitChance(level)
    };
    if(!result.upgradeable){
        return result;
    };
    result.exp += increase;
    while(result.exp >= result.max){
        if(!result.upgradeable){
            break;
        };
        result.exp = result.exp - result.max;
        result.level +=1;
        result.max = maxExp[result.level] || 0;
        result.upgradeable = maxExp[result.level];
        result.unitChance = getUnitChance(result.level);
    };

    return result;
};

export {getExp,getUnitChance};