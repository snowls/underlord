class Unit{
    cost = 1;
    constructor(unit){
        this.name = "未知";
        this.level = 1;
        this.nextLevel = null; //下一个升级对象
        this.hp = 650; //血量
        this.mp = 100; //魔法
        this.hpMax = this.hp; //最大血量
        this.mpMax = this.mp; //最大魔法
        this.attack = 60; //攻击力
        this.attackSpeed = 1; //攻击频率
        this.attackType = 0; //攻击类型 0近战 1远程
        this.attackDistance = 1; //攻击距离
        this.defense = 5; //防御力
        this.move = 300; //移动速度
        this.recover = 5; //回血速度 /s
        this.magicResistance = 0; //魔法抗性
        this.equip = []; //装备
        this.yoke = []; //羁绊
        this.skill = []; //技能
        this.maxEquipNum = 1; //最大携带装备数量
        if(unit){
            this.cost = unit.cost;
        };
    };

    battleCallback= []; //战斗开始前的回调 添加羁绊buff
    battleResumeCallback = [];//战斗结束后的回调 清除羁绊buff
    battleItemCallback = []; //道具的战斗开始时回调


    updateBattleCallback(fnArr){
        this.battleCallback = fnArr;
    };
    updateResumeBattleCallback(fnArr){
        this.battleResumeCallback = fnArr;
    };
    
    updateItemBattleCallback(){
        var result = [];
        this.equip.forEach(v=>{
            result.push(v.battleCallback);
        });
        this.battleItemCallback = result;
    };

    getSellPrice(){
        return this.level === 1 ? this.cost : Math.floor(this.cost * this.level * 3 * 0.9);
    };
    setPrice(num){
        this.cost = num;
    };
};
export {Unit};