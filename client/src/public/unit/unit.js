import {Juyahaimin} from './juyahaimin.js';
import {Bianfuqishi} from './bianfuqishi.js';
import {Fuwang} from './fuwang.js';
import {Xiaoxiao} from './xiaoxiao.js';
import {Anyingsaman} from './anyingsaman.js';

function setPrice(arr,price){
    return arr.map(v=>{
        return ()=>{
            var cache = new v();
            cache.setPrice(price);
            return cache;
        };
    });
};

var unitStep1 = setPrice([
    Juyahaimin,Bianfuqishi,Fuwang,Xiaoxiao,Anyingsaman
],1);
var unitStep2 = setPrice([Juyahaimin],2);
var unitStep3 = setPrice([Juyahaimin],3);
var unitStep4 = setPrice([Juyahaimin],4);
var unitStep5 = setPrice([Juyahaimin],5);

export {unitStep1,unitStep2,unitStep3,unitStep4,unitStep5}; 