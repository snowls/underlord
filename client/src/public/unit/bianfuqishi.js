import {Unit} from './common.js';
import {Soldier} from '../yoke/soldier.js';
class Bianfuqishi extends Unit{
    constructor(){
        super();
        this.name = "蝙蝠骑士";
        this.level = 1;
        this.nextLevel = Bianfuqishi2;
        this.hp = 650; //血量上限
        this.mp = 100; //魔法上限
        this.attack = 60; //攻击力
        this.defense = 5; //防御力
        this.move = 300; //移动速度
        this.attackSpeed = 1; //攻击频率
        this.recover = 5; //回血速度 /s
        this.magicResistance = 0; //魔法抗性
        this.attackDistance = 1; //攻击距离
        this.attackType = 0; //攻击类型 0近战 1远程
        this.equip = []; //装备
        this.yoke = [new Soldier(this)]; //羁绊
    };
};

class Bianfuqishi2 extends Unit{
    constructor(prevObj){
        super();
        this.name = "蝙蝠骑士";
        this.level = 2;
        this.nextLevel = Bianfuqishi3;
        this.hp = 650; //血量上限
        this.mp = 100; //魔法上限
        this.attack = 60; //攻击力
        this.defense = 5; //防御力
        this.move = 300; //移动速度
        this.attackSpeed = 1; //攻击频率
        this.recover = 5; //回血速度 /s
        this.magicResistance = 0; //魔法抗性
        this.attackDistance = 1; //攻击距离
        this.attackType = 0; //攻击类型 0近战 1远程
        // this.equip = prevObj ? prevObj.equip : []; //装备
        this.equip = [];
        this.yoke = [new Soldier(this)]; //羁绊
    };
};

class Bianfuqishi3 extends Unit{
    constructor(prevObj){
        super();
        this.name = "蝙蝠骑士";
        this.level = 3;
        this.nextLevel = null;
        this.hp = 650; //血量上限
        this.mp = 100; //魔法上限
        this.attack = 60; //攻击力
        this.defense = 5; //防御力
        this.move = 300; //移动速度
        this.attackSpeed = 1; //攻击频率
        this.recover = 5; //回血速度 /s
        this.magicResistance = 0; //魔法抗性
        this.attackDistance = 1; //攻击距离
        this.attackType = 0; //攻击类型 0近战 1远程
        // this.equip = prevObj ? prevObj.equip : []; //装备
        this.equip = [];
        this.yoke = [new Soldier(this)]; //羁绊
    };
};

export {Bianfuqishi};